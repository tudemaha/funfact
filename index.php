<?php

if(!isset($_GET["page"])) {
    $_GET["page"] = "0";
    $link = "1";
    $text = "<b>Hai!!</b><br>
    Aku ada funfact nih!<br><br>
    Siap? Coba klik Next.";
} else {
    switch($_GET["page"]) {
        case("1"):
            $link = "2";
            $text = "Percaya ga?<br>
            Kalo lagi <b>senyum</b>, kita ga bisa nafas...<br><br>
            Udah cobain? Coba klik next lagi.";
            break;
        
        case("2"):
            $link = "3";
            $text = "Nah gitu dong!<br><br>
            Kan cantik...💖";
            break;
    
        case("3"):
            $link = "4";
            $text = "<h6>Makasih yaa udah nyobain fun factnyaa😊</h6>";
            break;
    
        default:
            $link = "99";
            $text = "";
    }
}

?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fun Fact!</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
  </head>
  <body>
    <div class="black">
        <div class="container vh-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-md-4">
                    <div class="card">
                        <h5 class="card-header">Fun Fact!</h5>
                        <div class="card-body">
                                <?php if($link == "4") { ?>
                                    <p class="card-text">
                                        <div class="text-center"><?= $text ?></div>
                                    </p>
                                <?php } else { ?>
                                    <p class="card-text"><?= $text ?></p>
                                <?php } ?>
                            <div class="row justify-content-end">
                                <div class="col-2 me-4">
                                    <?php
                                    if($link < "4") {
                                    ?>
                                        <a href="index.php?page=<?= $link ?>" class="btn btn-info">Next</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>